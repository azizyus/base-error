var v = {

  methods: {

    hasErrors (key, errors) {
      return (key in errors)
    },

    errorsInline (key, errors) {
      // var exampleErrors = {"message":"The given data was invalid.","errors":{"name":["The name must be a string."],"email":["The email must be a valid email address."],"password":["The password field is required."],"storeName":["The store name field is required."]}};
      return errors[key].join('<br>')
    },

    formElementClassComp (key, errors) {
      if (this.hasErrors(key, errors)) {
        return 'form-element error'
      }
      return 'form-element'
    }
  }

}
export default v
